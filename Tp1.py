# Travail - Feuille de temps
# Cours   - INF3005  Programmation web avancée
# Session - Hiver 2018  UQAM
# Auteure - Myrianne Beaudoin-Thériault  BEAM26588114

# --------------------------------------------------------------IMPORTS

from flask import Flask
from flask import render_template
from flask import redirect
from flask import request
from flask import g
from flask import url_for
from .database import Database

import datetime
import re
import calendar
import sys

# --------------------------------------------------------------CONSTANTS

calendar.setfirstweekday(calendar.SUNDAY)
app = Flask(__name__)

months = {
    1: 'Janvier',
    2: 'Février',
    3: 'Mars',
    4: 'Avril',
    5: 'Mai',
    6: 'Juin',
    7: 'Juillet',
    8: 'Août',
    9: 'Septembre',
    10: 'Octobre',
    11: 'Novembre',
    12: 'Décembre',
    }


# --------------------------------------------------------------F(X)S

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        g._database = Database()
    return g._database


def validate_id(matricule):
    if len(matricule) is 6 and \
            re.match('[A-Z]{3}[-][0-9]{2}', matricule, flags=0) and \
            get_db().get_matricule(matricule) is not None:
        return True
    else:
        return False


def validate_date(date_du_jour):
    date = date_du_jour.split('-')
    try:
        datetime.datetime(int(date[0]), int(date[1]), int(date[2]),
                          0, 0, 0, 0)
    except ValueError:
        return False
    except IndexError:
        return False
    if len(date_du_jour) is 10 \
        and re.match('[0-9]{4}[-][0-9]{2}[-][0-9]{2}', date_du_jour,
                     flags=0):
        return True
    else:
        return False


def validate_hours(hours):
    try:
        val = int(hours)
    except ValueError:
        return False
    if int(hours) < 1:
        return False
    return True


def get_last_date(date):
    dates = date.split('-')
    date_obj = datetime.datetime(int(dates[0]), int(dates[1]),
                                 int(dates[2]), 0, 0, 0, 0)
    last_day = date_obj - datetime.timedelta(days=1)
    new_date = last_day.strftime('%Y-%m-%d')
    return new_date


def get_next_date(date):
    dates = date.split('-')
    date_obj = datetime.datetime(int(dates[0]), int(dates[1]),
                                 int(dates[2]), 0, 0, 0, 0)
    next_day = date_obj + datetime.timedelta(days=1)
    new_date = next_day.strftime('%Y-%m-%d')
    return new_date


def get_next_month(mois):
    infos = mois.split('-')
    date_obj = datetime.datetime(int(infos[0]), int(infos[1]),
                                 int('01'), 0, 0, 0, 0)
    next_month = date_obj + datetime.timedelta(days=31)
    new_month = next_month.strftime('%Y-%m')
    return new_month


def get_last_month(mois):
    infos = mois.split('-')
    date_obj = datetime.datetime(int(infos[0]), int(infos[1]),
                                 int('01'), 0, 0, 0, 0)
    last_month = date_obj - datetime.timedelta(days=28)
    new_month = last_month.strftime('%Y-%m')
    return new_month


def get_total_hours(monthly_hours):
    if monthly_hours == 0:
        return '0 heure'
    if monthly_hours > 0:
        return str(monthly_hours) + ' heures'


# --------------------------------------------------------------ROUTES

# ---------------------------------------------------------------------(ACCUEIL)

@app.errorhandler(404)
def page_not_found(e):
    return (render_template('404.html'), 404)


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.disconnect()


@app.route('/')
def accueil():
    return render_template('accueil.html')


@app.route('/send', methods=['POST'])
def infos_accueil():
    matricule = request.form['matriculeText']
    date_du_jour = datetime.datetime.today().strftime('%Y-%m-%d')
    if validate_id(matricule):
        return redirect(url_for('date_du_jour', matricule=matricule,
                        date_du_jour=date_du_jour))
    else:
        print("NOT VALID!")
        return render_template('accueil.html',
                               erreur='matricule inexistant!')


# ---------------------------------------------------------------------(DATE)

@app.route('/<matricule>/<date_du_jour>')
def date_du_jour(matricule, date_du_jour):
    if validate_id(matricule) and validate_date(date_du_jour):
        url = request.url
        date_url = url.split('/')[4]
        date_new = date_url.split('?')[0]
        hours = get_db().get_hours_for_date(matricule, date_new)
        total = get_db().get_total_hours_for_date(matricule, date_new)
        if total == 0:
            return render_template('date_du_jour.html',
                                   matricule=matricule,
                                   date_du_jour=date_new,
                                   erreur3=" Il n'y a aucune heure"
                                   " pour cette date")
        return render_template('date_du_jour.html',
                               matricule=matricule,
                               date_du_jour=date_new, hours=hours,
                               total='total: ' + str(total) + ' heures')
    else:
        return (render_template('404.html'), 404)


@app.route('/<matricule>/<date_du_jour>/ajouter', methods=['GET', 'POST'])
def ajout_date_du_jour(matricule, date_du_jour):
    if validate_id(matricule) and validate_date(date_du_jour):
        code = request.form['code_de_projet']
        hours = request.form['nb_heures']
        if (validate_hours(hours)):
            total_hours = get_db().add_hours(matricule, date_du_jour, code,
                                             hours)
            if total_hours == 0:
                hours = get_db().get_hours_for_date(matricule, date_du_jour)
                total = get_db().get_total_hours_for_date(matricule,
                                                          date_du_jour)
                if total == 0:
                    return render_template(
                        'date_du_jour.html',
                        matricule=matricule,
                        date_du_jour=date_du_jour,
                        hours=hours, erreur='Nice try!',
                        erreur2="Il n'y a que 24 heures dans une journée.",
                        erreur3="Il n'y a aucune heure pour cette date.")
                return render_template(
                    'date_du_jour.html',
                    matricule=matricule,
                    date_du_jour=date_du_jour,
                    hours=hours,
                    total='total: ' + str(total) + ' heures',
                    erreur='Nice try!',
                    erreur2="Il n'y a que 24 heures dans une journée.")
            else:
                return redirect(url_for('date_du_jour',
                                matricule=matricule,
                                date_du_jour=date_du_jour))
        else:
            return redirect(url_for('date_du_jour', matricule=matricule,
                                    date_du_jour=date_du_jour))
    else:
        return (render_template('404.html'), 404)


@app.route('/<matricule>/<date_du_jour>/modifier', methods=['GET', 'POST'])
def modif_date_du_jour(matricule, date_du_jour):
    if validate_id(matricule) and validate_date(date_du_jour):
        code = request.form['code_de_projet']
        hours = request.form['nb_heures']
        if (validate_hours(hours)):
            if get_db().get_hours_for_code(matricule, code,
                                           date_du_jour) is None:
                hours = get_db().get_hours_for_date(matricule, date_du_jour)
                total = get_db().get_total_hours_for_date(matricule,
                                                          date_du_jour)
                if total == 0:
                        return render_template(
                            'date_du_jour.html',
                            matricule=matricule,
                            date_du_jour=date_du_jour,
                            erreur='code inexistant!',
                            erreur3="Il n'y a aucune heure pour cette date.")
                return render_template(
                    'date_du_jour.html',
                    matricule=matricule,
                    date_du_jour=date_du_jour,
                    hours=hours,
                    code_de_projet=code,
                    total='total: ' + str(total) + ' heures',
                    erreur='code inexistant!')
            else:
                get_db().modify_hours(matricule, date_du_jour, code, hours)
                return redirect(url_for('date_du_jour',
                                matricule=matricule,
                                date_du_jour=date_du_jour))
        else:
            return redirect(url_for('date_du_jour', matricule=matricule,
                                    date_du_jour=date_du_jour))
    else:
        return (render_template('404.html'), 404)


@app.route('/<matricule>/<date_du_jour>/supprimer', methods=['GET',
           'POST'])
def supprimer_date_du_jour(matricule, date_du_jour):
    if validate_id(matricule) and validate_date(date_du_jour):
        code = request.form['code_de_projet']
        if get_db().get_hours_for_code(matricule, code, date_du_jour) is None:
            hours = get_db().get_hours_for_date(matricule, date_du_jour)
            total = get_db().get_total_hours_for_date(matricule, date_du_jour)
            return render_template(
                'date_du_jour.html',
                matricule=matricule,
                date_du_jour=date_du_jour,
                hours=hours,
                total='total: ' + str(total) + ' heures',
                erreur='code inexistant!')
        else:
            get_db().delete_hours(matricule, date_du_jour, code)
            return redirect(url_for('date_du_jour',
                                    matricule=matricule,
                                    date_du_jour=date_du_jour))
    else:
        return (render_template('404.html'), 404)


@app.route('/<matricule>/<date_du_jour>/reculer', methods=['GET', 'POST'])
def get_last_date_du_jour(matricule, date_du_jour):
    if validate_id(matricule) and validate_date(date_du_jour):
        new_date = get_last_date(date_du_jour)
        hours = get_db().get_hours_for_date(matricule, new_date)
        total = get_db().get_total_hours_for_date(matricule, new_date)
        return redirect(url_for('date_du_jour', matricule=matricule,
                        date_du_jour=new_date))
    else:
        return (render_template('404.html'), 404)


@app.route('/<matricule>/<date_du_jour>/avancer', methods=['GET', 'POST'])
def get_next_date_du_jour(matricule, date_du_jour):
    if validate_id(matricule) and validate_date(date_du_jour):
        new_date = get_next_date(date_du_jour)
        hours = get_db().get_hours_for_date(matricule, new_date)
        total = get_db().get_total_hours_for_date(matricule, new_date)
        return redirect(url_for('date_du_jour', matricule=matricule,
                        date_du_jour=new_date))
    else:
        return (render_template('404.html'), 404)


# -------------------------------------------------------------------------(MOIS)

@app.route('/<matricule>/overview/<mois>/avancer', methods=['GET', 'POST'])
def avancer_mois(matricule, mois):
    if validate_id(matricule) and int(mois.split('-')[1]) > 0 \
            and int(mois.split('-')[1]) < 13:
        next_month = get_next_month(mois)
        return redirect(url_for('mois', matricule=matricule,
                        mois=next_month))
    else:
        return (render_template('404.html'), 404)


@app.route('/<matricule>/overview/<mois>/reculer', methods=['GET', 'POST'])
def reculer_mois(matricule, mois):
    if validate_id(matricule) and int(mois.split('-')[1]) > 0 \
            and int(mois.split('-')[1]) < 13:
        last_month = get_last_month(mois)
        return redirect(url_for('mois', matricule=matricule,
                        mois=last_month))
    else:
        return (render_template('404.html'), 404)


@app.route('/<matricule>/overview/<mois>', methods=['GET', 'POST'])
def mois(matricule, mois):
    if validate_id(matricule) and int(mois.split('-')[1]) > 0 \
            and int(mois.split('-')[1]) < 13 and len(mois) == 7:
        entrySliced = mois.split('-')
        year = entrySliced[0]
        month = entrySliced[1]
        frenchMonth = months[int(month)]
        month_cal = []
        hours = []
        daily_hours = 0
        monthly_hours = 0
        cal = calendar.Calendar()
        for x in cal.itermonthdates(int(year), int(month)):
            if x.strftime('%m') == month:
                day = x.strftime('%d')
                month_cal.append(day)
                date = year + '-' + month + '-' + day
                daily_hours = \
                    get_db().get_total_hours_for_date(matricule, date)
                if daily_hours == 0:
                    hours.append('--')
                else:
                    daily = str(daily_hours) + 'h'
                    monthly_hours += daily_hours
                    hours.append(daily)
            else:
                month_cal.append('')
                hours.append('')
        total_hours = get_total_hours(monthly_hours)
        return render_template(
            'mois.html',
            matricule=matricule, year=year, mois=mois,
            month=frenchMonth, calendar=month_cal,
            hours=hours, total=total_hours)
    else:
        return (render_template('404.html'), 404)


# ---------------------------------------------------------------------(MATRICULE)

@app.route('/<matricule>', methods=['GET', 'POST'])
def matricule(matricule):
    if validate_id(matricule):
        dates = get_db().get_months_worked(matricule)
        months = []
        last_month = 0
        first_month = True
        for x in dates:
            infos = str(x[0]).split('-')
            month = infos[0] + '-' + infos[1]
            if last_month != month and month not in months:
                months.append(month)
            last_month = month
            first_month = False
        months.sort(reverse=True)
        return render_template('matricule.html', matricule=matricule,
                               months=months)
    else:
        return (render_template('404.html'), 404)
