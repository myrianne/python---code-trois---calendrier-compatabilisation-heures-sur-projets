# Travail - Feuille de temps
# Cours   - INF3005  Programmation web avancée
# Session - Hiver 2018  UQAM
# Auteure - Myrianne Beaudoin-Thériault  BEAM26588114

# ----------------------------------------------------------IMPORTS

import sqlite3

# ----------------------------------------------------------CONNEXION


class Database:

    def __init__(self):
        self.connection = None

    def get_connection(self):
        if self.connection is None:
            self.connection = sqlite3.connect('db/db.db')
        return self.connection

    def disconnect(self):
        if self.connection is not None:
            self.connection.close()

# ----------------------------------------------------------MATRICULE

    def get_matricule(self, matricule):
        cursor = self.get_connection().cursor()
        cursor.execute('''SELECT * FROM heures
                        WHERE matricule LIKE '%s';''' % matricule)
        infos = cursor.fetchall()
        if len(infos) == 0:
            infos = None
        return infos

# ----------------------------------------------------------HOURS

    def get_hours_for_date(self, matricule, date):
        cursor = self.get_connection().cursor()
        cursor.execute('''SELECT * FROM heures
                        WHERE matricule LIKE '%s'
                        AND date_publication LIKE '%s';'''
                       % (matricule, date))
        hours = cursor.fetchall()
        return hours

    def get_hours_for_code(self, matricule, code, date):
        cursor = self.get_connection().cursor()
        cursor.execute('''SELECT * FROM heures
                        WHERE matricule LIKE '%s'
                        AND code_de_projet LIKE '%s'
                        AND date_publication LIKE '%s';'''
                       % (matricule, code, date))
        hours = cursor.fetchone()
        return hours

    def get_total_hours_for_date(self, matricule, date):
        cursor = self.get_connection().cursor()
        cursor.execute('''SELECT SUM(duree) AS hours FROM heures
                        WHERE matricule LIKE '%s'
                        AND date_publication LIKE '%s';'''
                       % (matricule, date))
        hours = cursor.fetchone()
        if len(hours) == 0 or hours[0] is None:
            return 0
        else:
            return hours[0]

# ----------------------------------------------------------CHANGE HOURS

    def modify_hours(self, matricule, date, code, hours):
        connection = self.get_connection()
        cursor = connection.cursor()
        cursor.execute('''UPDATE heures SET duree=%d
                        WHERE matricule LIKE '%s'
                        AND code_de_projet LIKE '%s'
                        AND date_publication LIKE '%s' ;'''
                       % (int(hours), matricule, code, date))
        connection.commit()

    def add_hours(self, matricule, date, code, hours):
        hours_for_day = int(hours) \
            + self.get_total_hours_for_date(matricule, date)
        hours_for_code = self.get_hours_for_code(matricule, code, date)

        if hours_for_day > 24:
            return 0
        if hours_for_code is None:
            connection = self.get_connection()
            cursor = connection.cursor()
            cursor.execute('''INSERT INTO heures(matricule, code_de_projet,
                           date_publication, duree) values(?,?,?,?);''',
                           (matricule, code, date, int(hours)))
            connection.commit()
            return int(hours)
        else:
            hours_to_set = hours_for_code[0] + int(hours)
            self.modify_hours(matricule, date, code, hours_for_day)
            return hours_for_day

    def delete_hours(self, matricule, date, code):
        connection = self.get_connection()
        cursor = connection.cursor()
        cursor.execute('''DELETE FROM heures
                        WHERE matricule LIKE '%s'
                        AND code_de_projet LIKE '%s'
                        AND date_publication LIKE '%s';'''
                       % (matricule, code, date))
        connection.commit()

# ----------------------------------------------------------MONTHS

    def get_months_worked(self, matricule):
        cursor = self.get_connection().cursor()
        cursor.execute('''SELECT DISTINCT date_publication FROM heures
                        WHERE matricule LIKE '%s';''' % matricule)
        months = cursor.fetchall()
        return months
